import { IMAGE_DATA, TEXT_DATA, ADD_COMMENT_TEXT, ADD_LIKE_TEXT, ADD_COMMENT_IMAGE, ADD_LIKE_IMAGE } from '../action/types';

const initialState = {
    textData: [
        {
        "item_description": "",
        "image" : "",
        "likes" : 0,
        "comments" : [],
        }
    ],
    imageData: [
        {
        "item_description": "",
        "image" : "",
        "likes" : 0,
        "comments" : [],
        }
    ],
}

export default function(state = initialState, action) {
    switch (action.type) {
        case TEXT_DATA:
            return {
                ...state,
                textData: action.payload
            }
        case IMAGE_DATA:
            return {
                ...state,
                imageData: action.payload
            }
        case ADD_COMMENT_TEXT:
            const temp1 = [...state.textData];
            temp1[0].comments.push({comment:action.payload});
            return {
                ...state,
                textData: temp1,
            }
        case ADD_LIKE_TEXT:
            const tempData1 = [...state.textData];
            tempData1[0].likes = tempData1[0].likes+1;
            return {
                ...state,
                textData: tempData1,
            }
        case ADD_COMMENT_IMAGE:
            const temp2 = [...state.imageData];
            temp2[0].comments.push({comment:action.payload});
            return {
                ...state,
                imageData: temp2,
            }
        case ADD_LIKE_IMAGE:
            const tempData2 = [...state.imageData];
            tempData2[0].likes = tempData2[0].likes+1;
            return {
                ...state,
                imageData: tempData2,
            }
        default:
            return state; 
    }
}