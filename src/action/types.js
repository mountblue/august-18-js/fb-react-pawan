export const TEXT_DATA = 'TEXT_DATA';
export const IMAGE_DATA = 'IMAGE_DATA';
export const ADD_COMMENT_TEXT = 'ADD_COMMENT_TEXT';
export const ADD_LIKE_TEXT = 'ADD_LIKE_TEXT';
export const ADD_COMMENT_IMAGE = 'ADD_COMMENT_IMAGE';
export const ADD_LIKE_IMAGE = 'ADD_LIKE_IMAGE';