import { IMAGE_DATA, TEXT_DATA, ADD_COMMENT_TEXT, ADD_LIKE_TEXT, ADD_COMMENT_IMAGE, ADD_LIKE_IMAGE} from './types';
import data from '../facebook.json';
 
export const filterTextData = function(searchDetails) {
    const filterData = data.facebookData.filter(item => item.item_description == searchDetails)
    return ({
        type: TEXT_DATA,
        payload: filterData,
    })
}
export const filterImageData = function(searchDetails) {
    const filterData = data.facebookData.filter(item => item.item_description == searchDetails)
    return ({
        type: IMAGE_DATA,
        payload: filterData,
    })
}
export const addCommentOfText = function(commentInput) {
    return ({
        type: ADD_COMMENT_TEXT,
        payload: commentInput,
    })
}
export const addLikeOfText = function() {
    return ({
        type: ADD_LIKE_TEXT,
        payload: 1,
    })
}
export const addCommentOfImage = function(commentInput) {
    return ({
        type: ADD_COMMENT_IMAGE,
        payload: commentInput,
    })
}
export const addLikeOfImage = function() {
    return ({
        type: ADD_LIKE_IMAGE,
        payload: 1,
    })
}