import React, { Component } from 'react';
import { Button, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { filterTextData, addCommentOfText, addLikeOfText } from '../../action/allAction';
import { filterImageData, addCommentOfImage, addLikeOfImage } from '../../action/allAction';

class TextAndImage extends Component {
  constructor(props){
    super();
    this.state = {
      inputCommentText: '',
      inputCommentImage: '',
    }
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.onSubmitText = this.onSubmitText.bind(this);
    this.onSubmitImage = this.onSubmitImage.bind(this);
    this.onClick = this.onClick.bind(this);
  }
  
  componentWillMount() {
    this.props.filterTextData("This is a text only item");
    this.props.filterImageData("This is an item with an image");
  }

  onChangeText(e){
    this.setState({inputCommentText:e.target.value});
  }
  onChangeImage(e){
    this.setState({inputCommentImage:e.target.value});
  }

  onSubmitText(e){
    e.preventDefault();
    this.props.addCommentOfText(this.state.inputCommentText);
  }
  onSubmitImage(e){
    e.preventDefault();
    this.props.addCommentOfImage(this.state.inputCommentImage);
  }

  onClick(e){
    e.preventDefault();
    if(e.target.name === "textLike"){
      this.props.addLikeOfText();
    }
    if(e.target.name === "imageLike"){
      this.props.addLikeOfImage();
    }
  }

  render() {
    const textLikes = this.props.textData.map(item => 
      <h5 key={Math.random()}> likes({item.likes})</h5>
      )
    const textComment = this.props.textData.map(item => item.comments)
    const textComments = textComment[0].map(item => 
      <h5 key={Math.random()}>{item.comment}</h5>
      )

    const imageLikes = this.props.imageData.map(item => 
      <h5 key={Math.random()}> likes({item.likes})</h5>
      )
    const imageComment = this.props.imageData.map(item => item.comments)
    const imageComments = imageComment[0].map(item => 
      <h5 key={Math.random()}>{item.comment}</h5>
      )
    return (
      <div className="image">
        <div className="text_only">
          <h4>This is text only feed item</h4>
          {textLikes}
          <Button bsStyle="primary" name="textLike" onClick={this.onClick}>Like button</Button>
        </div>
        <div className="comment">
        <form name="text" onSubmit={this.onSubmitText}>
          <h4>Comments</h4>
          {textComments}
          <FormControl id="formControlsText" type="text" name="textComment" placeholder="Reply to comment" onChange={this.onChangeText}/>
        </form>
        </div>
        <div className="image_only">
          <h4>Image</h4>
          {imageLikes}
          <Button bsStyle="primary" name="imageLike" onClick={this.onClick}>Like button</Button>
        </div>
        <div className="image_comment">
        <form name="image" onSubmit={this.onSubmitImage}>
          <h4>Comments</h4>
          {imageComments}
          <FormControl id="formControlsText" type="text" name="imageComment" placeholder="Reply to comment" onChange={this.onChangeImage}/>
        </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  textData:state.allReducer.textData,
  imageData:state.allReducer.imageData,
})

export default connect(mapStateToProps,
  { filterTextData,filterImageData,addCommentOfImage,addLikeOfImage,addCommentOfText,addLikeOfText })
  (TextAndImage);
