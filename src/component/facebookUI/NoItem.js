import React, { Component } from 'react';

class NoItem extends Component {
  render() {
    return (
      <div className="noItem">
        <h3>No Item available</h3>
      </div>
    );
  }
}

export default NoItem;
