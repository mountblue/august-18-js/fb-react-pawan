import React, { Component } from 'react';
import { ControlLabel, FormControl } from 'react-bootstrap';

class SelectType extends Component {
  constructor(props){
      super();
      this.onChange = this.onChange.bind(this);
  }
  
  onChange(e){
      if(e.target.value === 'Text'){
        this.props.history.push("/Text_only/");
      }
      if(e.target.value === 'Image'){
        this.props.history.push("/Image_only/");
      }
      if(e.target.value === 'Image_and_text'){
        this.props.history.push("/Image_and_Text/");
      }
      if(e.target.value === 'No_Items'){
        this.props.history.push("/No_Item/");
      }

  }
  render() {
    return (
      <div className="type">
        <form>
            <ControlLabel>Select type:</ControlLabel><br/>
            <FormControl componentClass="select" name="type" onChange={this.onChange}>
              <option value="select">Select type</option>
              <option value="Text">Text only</option>
              <option value="Image">Image only</option>
              <option value="Image_and_text">Image and text</option>
              <option value="No_Items">No Items</option>
            </FormControl>
        </form>
      </div>
    );
  }
}

export default SelectType;
