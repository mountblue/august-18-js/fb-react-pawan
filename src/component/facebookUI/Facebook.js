import React, { Component } from 'react';
import { Button, ControlLabel, FormControl } from 'react-bootstrap';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import SelectType from './SelectType';
import Text from './Text';
import Image from './Image';
import TextAndImage from './TextAndImage';
import NoItem from './NoItem';


class Facebook extends Component {

  render() {
    return (
      <Router>
        <div className="facebook">
          <h1>Facebook UI</h1>
          <Route path="/" render={(props)=> <SelectType {...props} />} /> 
          <Route path="/Text_only/" exact render={(props)=> <Text {...props}/>} />
          <Route path="/Image_only/" exact render={(props)=> <Image {...props}/>} />
          <Route path="/Image_and_Text/" exact render={(props)=> <TextAndImage {...props} />} />
          <Route path="/No_Item/" exact render={(props)=> <NoItem {...props} />} />
        </div>
      </Router>
    );
  }
}

export default Facebook;
