import React, { Component } from 'react';
import { Button, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { filterImageData, addCommentOfImage, addLikeOfImage } from '../../action/allAction';

class Image extends Component {
  constructor(props){
    super();
    this.state = {
      inputComment: '',
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmitComment = this.onSubmitComment.bind(this);
    this.onClick = this.onClick.bind(this);
  }
  
  componentWillMount() {
    this.props.filterImageData("This is an item with an image");
  }

  onChange(e){
    this.setState({inputComment:e.target.value});
  }

  onSubmitComment(e){
    e.preventDefault();
    this.props.addCommentOfImage(this.state.inputComment);
  }

  onClick(e){
    e.preventDefault();
    this.props.addLikeOfImage();
  }

  render() {
    const likes = this.props.imageData.map(item => 
      <h5 key={item.likes}> likes({item.likes})</h5>
      )
    const comment = this.props.imageData.map(item => item.comments)
    const comments = comment[0].map(item => 
      <h5 key={item.comment}>{item.comment}</h5>
    )
    return (
      <div className="image">
        <div className="image_only">
          <h4>This is an item with an image</h4>
          {likes}
          <Button bsStyle="primary" onClick={this.onClick}>Like button</Button>
        </div>
        <div className="image_comment">
        <form onSubmit={this.onSubmitComment}>
          <h4>Comments</h4>
          {comments}
          <FormControl id="formControlsText" type="text" name="image_comment" placeholder="Reply to comment" onChange={this.onChange}/>
        </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  imageData:state.allReducer.imageData,
})

export default connect(mapStateToProps, { filterImageData, addCommentOfImage, addLikeOfImage })(Image);

