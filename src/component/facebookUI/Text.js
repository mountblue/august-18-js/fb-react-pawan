import React, { Component } from 'react';
import { Button, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { filterTextData, addCommentOfText, addLikeOfText } from '../../action/allAction';

class Text extends Component {
  constructor(props){
    super();
    this.state = {
      inputComment: '',
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmitComment = this.onSubmitComment.bind(this);
    this.onClick = this.onClick.bind(this);
  }
  
  componentWillMount() {
    this.props.filterTextData("This is a text only item");
  }

  onChange(e){
    this.setState({inputComment:e.target.value});
  }

  onSubmitComment(e){
    e.preventDefault();
    this.props.addCommentOfText(this.state.inputComment);
  }

  onClick(e){
    e.preventDefault();
    this.props.addLikeOfText();
  }

  render() {
    const likes = this.props.textData.map(item => 
      <h5 key={item.likes}> likes({item.likes})</h5>
      )
    const comment = this.props.textData.map(item => item.comments)
    const comments = comment[0].map(item => 
      <h5 key={item.comment}>{item.comment}</h5>
      )
    return (
      <div className="text">
        <div className="text_only">
          <h4>This is text only feed item</h4>
          {likes}
          <Button bsStyle="primary" onClick={this.onClick}>Like button</Button>
        </div>
        <div className="comment">
        <form onSubmit={this.onSubmitComment}>
          <h4>Comments:</h4>
          {comments}
          <FormControl id="formControlsText" type="text" name="comment" placeholder="Reply to comment" onChange={this.onChange}/>
        </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  textData:state.allReducer.textData,
})

export default connect(mapStateToProps, { filterTextData, addCommentOfText, addLikeOfText })(Text);


