import { createStore } from 'redux';
import allReducer from './reducer/allReducer';

const initialState = {};   

const store = createStore(allReducer, initialState)

export default store;