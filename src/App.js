import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './Store';
import Facebook from './component/facebookUI/Facebook';

class App extends Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <Provider store={store}>
        <div className="app">
        <Facebook />
        </div>
      </Provider>
    );
  }
}

export default App;

